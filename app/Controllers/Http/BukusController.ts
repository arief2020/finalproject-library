import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Buku from "App/Models/Buku";
import BukuValidator from "App/Validators/BukuValidator";

export default class BukusController {

  
  public async store({ request, response }: HttpContextContract) {
    const payload = await request.validate(BukuValidator);

    // payload.release_date = new Date(payload.release_date.toISOString())

    // const buku = await Database.table('books').insert(payload)
    const buku = await Buku.create({
      ...payload,
      release_date: new Date(payload.release_date.toSQLDate()),
    });

    if (buku) {
      return response.status(200).json({
        message: "success",
        data: payload,
      });
    }

    return response.badRequest({
      message: "Gagal menambahkan data buku",
    });
  }


  public async index({ response }: HttpContextContract) {
    const buku = await Buku.query().preload('category');

    if (buku) {
      return response.ok({
        message: "success",
        data: buku,
      });
    }

    return response.badRequest({
      message: "Gagal akses data buku",
    });
  }

  
  public async show({ response, params }: HttpContextContract) {
    const buku = await Buku.query().where('id', params.id).preload('category').first();

    if (buku) {
      return response.ok({
        message: "success",
        data: buku,
      });
    }

    return response.badRequest({
      message: `Gagal membaca data ${params.id} buku`,
    });
  }

  // public async update({response, request, params}: HttpContextContract){
  //   const payload = await request.validate(BukuValidator);

  //   const buku = await Buku.query().where('id', params.id).update(payload)

  //   if (buku) {
  //     return response.created({
  //       message: "success update data",
  //       // data: buku,
  //     });
  //   }

  //   return response.badRequest({
  //     message: `Gagal update data ${params.id} buku`,
  //   });
  // }
  
  

  public async update({ request, response, params }: HttpContextContract) {
    const payload = await request.validate(BukuValidator);

    // const category = await Database.from('kategoris').where('id', params.id).update(payload)
    const buku = await Buku.query()
      .where("id", params.id)
      .update({ ...payload, release_date: payload.release_date.toSQLDate() });

    if (buku) {
      return response.created({
        message: `berhasil update data ${params.id} buku`,
      });
    }
    return response.badRequest({
      message: "gagal update data",
    });
  }

  public async destroy({ response, params}: HttpContextContract){
    
    const buku =  await Buku.findByOrFail('id', params.id)
    if (buku) {
      await buku.delete()
      return response.ok({
        message: `berhasil hapus data ${params.id} buku`
        // data: buku
      });
    }
    return response.unprocessableEntity({
      message: "gagal hapus data",
    });
  }
}

import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Borrow from 'App/Models/Borrow'

export default class BorrowsController {
    public async store({request, response, params, auth}:HttpContextContract){
        const dataBooks = params.id
        const userData =  auth.user?.id

        const validateBorows = schema.create({
            return_date: schema.date({
                format: 'yyyy-MM-dd HH:mm:ss'
            }),
            loan_date: schema.date({
                format: 'yyyy-MM-dd HH:mm:ss'
            })
        })

        const payload = await request.validate({schema: validateBorows})

        await Borrow.create({
            user_id: userData,
            books_id: dataBooks,
            return_date: new Date(payload.return_date.toSQLDate()),
            loan_date: new Date(payload.loan_date.toSQLDate())
        })

        return response.ok({
            message: 'success borrows book'
        })
    }

    public async index({response}:HttpContextContract){
        const borrows = await Borrow.query().select(['id', 'books_id', 'loan_date', 'return_date', 'user_id']).preload('booksBorrow', query => query.select(['id', 'title', 'summary', 'release_date', 'stock'])).preload('userBorrow', query => query.select(['id', 'name', 'email']))

        if (borrows) {
            return response.ok({
                message: "success",
                data: borrows,
            });
        }
    return response.badRequest({
        message: "Gagal akses data peminjaman",
      });
    }

    public async show({response, params}:HttpContextContract){
        const borrows = await Borrow.query().select(['id', 'books_id', 'loan_date', 'return_date', 'user_id']).where('id', params.id).preload('booksBorrow', query => query.select(['id', 'title', 'summary', 'release_date', 'stock'])).preload('userBorrow', query => query.select(['id', 'name', 'email'])).first()

        // const firstBorrows = borrows[0]
        if (borrows) {
            return response.ok({
                message: `success access data ${params.id} borrows`,
                data: borrows
                
            })
        }
        return response.badRequest({message : 'gagal membaca data borrows'})
    }
}

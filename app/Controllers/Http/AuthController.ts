import Mail from '@ioc:Adonis/Addons/Mail'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Database from '@ioc:Adonis/Lucid/Database'
import User from 'App/Models/User'
import AuthValidator from 'App/Validators/AuthValidator'

export default class AuthController {
    public async register({response, request}:HttpContextContract){
        try {
            const DataValidation = await request.validate(AuthValidator)

            if (!DataValidation.hasOwnProperty('role') || !DataValidation['role']) {
                DataValidation['role'] = 'user'
              }
            const userCreate = await User.create(DataValidation)

            const otp_code = Math.floor(100000+Math.random() * 900000)

            await Database.table('otp_codes').insert({
                otp_code: otp_code,
                user_id: userCreate.id
            })
            await Mail.send((message) => {
                message
                  .from('admin@perpus.com')
                  .to(userCreate.email)
                  .subject('Verify OTP!')
                  .htmlView('emails/otp_verification', { otp_code })
              })
          

            return response.created({
                message: 'register success, verify otp'
            })
        } catch (error) {
            return response.unprocessableEntity({
                message: error
            })
        }
    }

    public async otpConfirmation({request, response}:HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

        if (user?.id == otpCheck.user_id) {
            user!.is_verified = true
            await user!.save()
            return response.status(200).json({message: 'berhasil konfirmasi OTP'})
        } else {
            return response.status(400).json({message: 'Gagal konfirmasi OTP'})
        }
    }

    public async login({request, response, auth}: HttpContextContract){
        try {
            const LoginValidation = schema.create({
                email: schema.string(),
                password: schema.string()
            })

            await request.validate({schema: LoginValidation})

            const email = request.input('email')
            const password = request.input('password')

            const token = await auth.use('api').attempt(email, password, {
                expiresIn: '7 days'
        })

        return response.ok({
            message: "Login berhasil", token
        })
        } catch (error) {
            if (error.guard) {
                return response.badRequest({
                    message: 'Login Validasi',
                    error: error.message
                })
            } else {
                return response.badRequest({
                    message: 'Login Gagal',
                    error: error.message
                })
            }
        }
    }

    public async updateProfile({request, auth, response}:HttpContextContract){
        const userData = auth.user

        const validationProfile = schema.create({
            alamat: schema.string(),
            bio: schema.string()
        })

        await request.validate({schema: validationProfile})

        const alamat = request.input('alamat')
        const bio = request.input('bio')

        const persistencePayload = {
            alamat, bio
        }

        await userData?.related('profile').updateOrCreate({}, persistencePayload)

        return response.ok({
            message: 'berhasil update atau membuat data'
        })
    }
}

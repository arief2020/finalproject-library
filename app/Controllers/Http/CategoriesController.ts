import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Category from 'App/Models/Category'
import CategoryValidator from 'App/Validators/CategoryValidator'

export default class CategoriesController {

    
    public async store ({request, response} : HttpContextContract){
        
        const payload = await request.validate(CategoryValidator)
        
        // const category = await Database.table('kategoris').insert(payload)
        const category = await Category.create(payload)

        if (category) {
            return response.status(200).json({
                message: "success",
                data: payload
            })
        }
        return response.badRequest({
            message: 'Gagal menambahkan genre'
        })
        
    }

    public async index({response}:HttpContextContract){
        const category = await Category.all()

        if (category) {
            return response.ok({
                message: "Akses data kategori",
                data: category
            })
        }
        return response.badRequest({
            message: "Gagal akses data kategori"
        })
    }
    
    public async show({response, params}:HttpContextContract){
        // const category = await Database.from('kategoris').where("id", params.id).first();
        const category = await Category.query().where('id', params.id).preload('Buku').first()

        // const firstCategory = category[0]

        if (category) {
            return response.ok({
                message: `success get data category by id`,
                data: category
            })
        }
        return response.badRequest({
            message: "gagal membaca data"
        })
    }

    public async update({request, response, params}:HttpContextContract){
        const payload = await request.validate(CategoryValidator);

        // const category = await Database.from('kategoris').where('id', params.id).update(payload)
        const category = await Category.query().where('id', params.id).update(payload)

        if (category) {
            return response.created({
                message :  `berhasil update data ${params.id} kategori`,
            })
        }
        return response.badRequest({
            message: "gagal update data"
        })
    }

    public async destroy({response, params}:HttpContextContract){
        const category = await Category.query().where('id', params.id).delete()

        if (category) {
            return response.ok({
                message: `data ${params.id} kategori berhasil dihapus`
            })

        }
        return response.badRequest({
            message: "data gagal dihapus"
        })
    }
}

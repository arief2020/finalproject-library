import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Admin {
  public async handle({response, auth}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL

    const admin = auth.user?.role == "admin"

    if (admin) {
      await next()
    }else{
      return response.unauthorized({
        message: "you don't have authority for this method or this url"
      })
    }
  }
}

import { DateTime } from "luxon";
import { BaseModel, BelongsTo, belongsTo, column } from "@ioc:Adonis/Lucid/Orm";
import User from "./User";
import Buku from "./Buku";

export default class Borrow extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public books_id: number

  @column()
  public user_id: number

  @column()
  public loan_date: Date

  @column()
  public return_date: Date

  @belongsTo(() => User, {
    foreignKey: 'user_id'
  })
  public userBorrow: BelongsTo<typeof User>

  @belongsTo(() => Buku, {
    foreignKey: 'books_id'
  })
  public booksBorrow: BelongsTo<typeof Buku>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}

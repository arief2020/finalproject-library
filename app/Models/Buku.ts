import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Category from './Category'
import User from './User'

export default class Buku extends BaseModel {
  public static table = 'books'
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column()
  public summary: string

  @column()
  public release_date: Date

  @column()
  public stock: number

  @column()
  public category_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Category, {
    foreignKey: 'category_id'
  })
  public category: BelongsTo<typeof Category>

  @manyToMany(() => User, {
    localKey: 'id',
    pivotForeignKey: 'books_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'user_id',
    pivotTable: 'borrows'

  })
  public user: ManyToMany<typeof User>
  

}

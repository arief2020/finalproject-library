import { DateTime } from 'luxon'
import { BaseModel, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Buku from './Buku'

export default class Category extends BaseModel {
  public static table = 'kategoris'
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Buku, {
    foreignKey: "category_id"
  })
  public Buku: HasMany<typeof Buku>

}

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.group(()=>{
  // Route.resource('category', 'CategoriesController').apiOnly().middleware({'index': ['auth', 'isAdmin']})
  Route.resource('category', 'CategoriesController').apiOnly().middleware({'*': ['auth', 'isAdmin']})
  Route.resource('books', 'BukusController').apiOnly().middleware({'*': ['auth'], 'create': ['isAdmin'], 'update': ['isAdmin'], 'destroy': ['isAdmin']})
  
  Route.post('/register', 'AuthController.register')
  Route.post('/login', 'AuthController.login')
  Route.post('/verify-otp', 'AuthController.otpConfirmation')
  Route.post('/profile', 'AuthController.updateProfile').middleware('auth')
  

  Route.get('/borrows', 'BorrowsController.index').middleware('auth')
  Route.get('/borrows/:id', 'BorrowsController.show').middleware('auth')
  Route.post('/buku/:id/borrows', 'BorrowsController.store').middleware('auth')

}).prefix('api')


// Route.post('kategori', 'CategoriesController.store')

// Route.post('buku', 'BukusController.store')